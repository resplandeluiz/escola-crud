@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
  <div class="card">
                <div class="card-header">Atualizar TURMA</div>

                <div class="card-body">
                    {{ Form::model('', array('route' => array('aluno.update', $aluno->id), 'method' => 'PUT')) }}

             {{ csrf_field() }}
                 <div class="form-group">
                <label for="exampleFormControlInput1">Nome</label>
                <input type="text" class="form-control" required placeholder="Nome do aluno" name="nm_aluno" value="{{$aluno->nome}}">
                
                
                 <label for="exampleFormControlInput1">Matricula</label>
                <input type="text" class="form-control" required placeholder="Matricula" name="nr_matricula" value="{{$aluno->matricula}}">
                
                <label for="exampleFormControlInput1">Idade</label>
                <input type="number" class="form-control" required placeholder="Idade" name="nr_idade" value="{{$aluno->idade}}">
                
             
                

	              <div class="form-group">
    <label for="exampleFormControlSelect1">Sexo</label>
    <select class="form-control" name="sexo">
      <option value="F">Feminino</option>
      <option value="M">Masculino</option>
      
    </select>
  </div>
	            
                  {{Form::label('id_turma','Turma')}}
	             {{ Form::select('id_turma',$turma_id,$aluno->turma_id,['class'=>'form-control required']) }}
               
	             
                    <input type="submit" value="Atualizar Aluno" class="btn btn-info white"> 
                    {{Form::close()}}
                </div>
            </div>
            </div></div>
            
            @endsection