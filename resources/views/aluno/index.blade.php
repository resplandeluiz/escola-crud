@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
  <div class="card">
                <div class="card-header">Cadastrar Aluno</div>

                <div class="card-body">
                    {{ Form::open(array('route'=>'aluno.store','method'=>'POST','id'=>'form')) }}

                 {{ csrf_field() }}
                 <div class="form-group">
                <label for="exampleFormControlInput1">Nome</label>
                <input type="text" class="form-control" required placeholder="Nome do aluno" name="nm_aluno">
                
                
                 <label for="exampleFormControlInput1">Matricula</label>
                <input type="text" class="form-control" required placeholder="Matricula" name="nr_matricula">
                
                <label for="exampleFormControlInput1">Idade</label>
                <input type="number" class="form-control" required placeholder="Idade" name="nr_idade">
                
             
                

	              <div class="form-group">
    <label for="exampleFormControlSelect1">Sexo</label>
    <select class="form-control" name="sexo">
      <option value="F">Feminino</option>
      <option value="M">Masculino</option>
      
    </select>
  </div>
	            
                  {{Form::label('id_turma','Turma')}}
	             {{ Form::select('id_turma',$turma_id,null,['class'=>'form-control required']) }}
               
                  </div>
                    <input type="submit" value="Cadastrar aluno" class="btn btn-info white"> 
                    {{Form::close()}}
                </div>
            </div>
            </div></div>
            
            
            <div class="row justify-content-center">
                <div class="col-md-8">
                      <div class="card">
                <div class="card-header">Turmas Cadastradas</div>

                <div class="card-body">
                    <table class="table">
  <thead>
    <tr>
     
      <th scope="col">Id</th>
      <th scope="col">Nome</th>
       <th scope="col">Turma</th>
      <th scope="col">Professor</th>
      <th scope="col">Disciplina</th>
      
      <th scope="col"></th>
      <th scope="col"></th>
    
    </tr>
  </thead>
  <tbody>
    @forelse($alunoComturma as $aluno)
    
    <tr>
      
      <td>{{$aluno->id}}</td>
      <td>{{$aluno->nome}}</td>
      <td>{{$aluno->nome_turma}}</td>
      <td>{{$aluno->professor_nome}}</td>
      <td>{{$aluno->nome_disciplina}}</td>
       <td>
          
          
          {{Form::open(['route'=>['aluno.edit',$aluno->id], 'method'=>'GET'])}}
           {{Form::submit('Editar', ['class'=>'btn btn-info btn-sm col-md-12'] )}}
  			{{Form::close()}}
        
    </td>
      <td>
          
          
          {{Form::open(['route'=>['aluno.destroy',$aluno->id], 'method'=>'DELETE'])}}
           {{Form::submit('Excluir', ['class'=>'btn btn-danger btn-sm col-md-12'] )}}
  			{{Form::close()}}
        
    </td>
          
          
      
      
    </tr>
    @empty
    
    Nenhum aluno cadastrado!
    
    @endforelse
  </tbody>
</table>
                </div>
                
            </div>
            </div></div>
            
            
            
            
            
            
            </div>

  
  @endsection