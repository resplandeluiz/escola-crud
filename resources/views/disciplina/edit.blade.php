@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
  <div class="card">
                <div class="card-header">Atualizar Disciplina</div>

                <div class="card-body">
                    {{ Form::model('', array('route' => array('disciplina.update', $disciplina->id), 'method' => 'PUT')) }}

                 {{ csrf_field() }}
                 
                 <div class="form-group">
                <label for="exampleFormControlInput1">Nome</label>
                <input type="text" class="form-control" required placeholder="Digite aqui o nome da disciplina" name="nm_disciplina" value="{{$disciplina->nome_disciplina}}">
                
               
                  </div>
                    <input type="submit" value="Atualizar Disciplina" class="btn btn-info white"> 
                    {{Form::close()}}
                </div>
            </div>
            </div></div>
            
            @endsection