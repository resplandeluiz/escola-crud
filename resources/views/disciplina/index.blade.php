@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
  <div class="card">
                <div class="card-header">Cadastrar Disciplina</div>

                <div class="card-body">
                    {{ Form::open(array('route'=>'disciplina.store','method'=>'POST','id'=>'form','enctype'=>'multipart/form-data')) }}

                 {{ csrf_field() }}
                 <div class="form-group">
                <label for="exampleFormControlInput1">Nome</label>
                <input type="text" class="form-control" required placeholder="Digite aqui o nome da disciplina" name="nm_disciplina">
                
                
              <input type="file" class="form-control" name="foto" id="exampleFormControlFile1" >
                
               
                  </div>
                    <input type="submit" value="Cadastrar Disciplina" class="btn btn-info white"> 
                    {{Form::close()}}
                </div>
            </div>
            </div></div>
            
            
            <div class="row justify-content-center">
                <div class="col-md-8">
                      <div class="card">
                <div class="card-header">Disciplinas Cadastradas</div>

                <div class="card-body">
                    <table class="table">
  <thead>
    <tr>
     
      <th scope="col">Id</th>
      <th scope="col">Nome</th>
      <th scope="col"></th>
      <th scope="col"></th>
    
    </tr>
  </thead>
  <tbody>
    @forelse($disciplinas as $disciplina)
    
    <tr>
      
      <td>{{$disciplina->id}}</td>
      <td>{{$disciplina->nome_disciplina}}</td>
       <td>
          
          
          {{Form::open(['route'=>['disciplina.edit',$disciplina->id], 'method'=>'GET'])}}
           {{Form::submit('Editar', ['class'=>'btn btn-info btn-sm col-md-12'] )}}
  			{{Form::close()}}
        
    </td>
      <td>
          
          
          {{Form::open(['route'=>['disciplina.destroy',$disciplina->id], 'method'=>'DELETE'])}}
           {{Form::submit('Excluir', ['class'=>'btn btn-danger btn-sm col-md-12'] )}}
  			{{Form::close()}}
        
    </td>
          
          
      
      
    </tr>
    @empty
    
    Nenhuma cadastrada!
    
    @endforelse
  </tbody>
</table>
                </div>
                
            </div>
            </div></div>
            
            
            
            
            
            
            </div>

  
  @endsection