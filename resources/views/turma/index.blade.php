@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
  <div class="card">
                <div class="card-header">Cadastrar Turma</div>

                <div class="card-body">
                    {{ Form::open(array('route'=>'turma.store','method'=>'POST','id'=>'form')) }}

                 {{ csrf_field() }}
                 <div class="form-group">
                <label for="exampleFormControlInput1">Nome</label>
                
                <input type="text" class="form-control" required placeholder="Nome da Turma" name="nm_turma">
                
                    {{Form::label('id_professor','Professor')}}
	             {{ Form::select('id_professor',$professor_id,null,['class'=>'form-control required' ]) }}
	             
	             
	             {{Form::label('id_disciplina','Disciplina')}}
	             {{ Form::select('id_disciplina',$disciplina_id,null,['class'=>'form-control required']) }}
	             
                
               
                  </div>
                    <input type="submit" value="Cadastrar Turma" class="btn btn-info white"> 
                    {{Form::close()}}
                </div>
            </div>
            </div></div>
            
            
            <div class="row justify-content-center">
                <div class="col-md-8">
                      <div class="card">
                <div class="card-header">Turmas Cadastradas</div>

                <div class="card-body">
                    <table class="table">
  <thead>
    <tr>
     
      <th scope="col">Id</th>
      <th scope="col">Nome</th>
      <th scope="col">Professor</th>
      <th scope="col">Disciplina</th>
      
      <th scope="col"></th>
      <th scope="col"></th>
    
    </tr>
  </thead>
  <tbody>
    @forelse($listTurmas as $listTurma)
    
    <tr>
      
      <td>{{$listTurma->id}}</td>
      <td>{{$listTurma->nome}}</td>
      <td>{{$listTurma->professor_nome}}</td>
      <td>{{$listTurma->nome_disciplina}}</td>
       <td>
          
          
          {{Form::open(['route'=>['turma.edit',$listTurma->id], 'method'=>'GET'])}}
           {{Form::submit('Editar', ['class'=>'btn btn-info btn-sm col-md-12'] )}}
  			{{Form::close()}}
        
    </td>
      <td>
          
          
          {{Form::open(['route'=>['turma.destroy',$listTurma->id], 'method'=>'DELETE'])}}
           {{Form::submit('Excluir', ['class'=>'btn btn-danger btn-sm col-md-12'] )}}
  			{{Form::close()}}
        
    </td>
          
          
      
      
    </tr>
    @empty
    
    Nenhuma turma cadastrada!
    
    @endforelse
  </tbody>
</table>
                </div>
                
            </div>
            </div></div>
            
            
            
            
            
            
            </div>

  
  @endsection