@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
  <div class="card">
                <div class="card-header">Atualizar TURMA</div>

                <div class="card-body">
                    {{ Form::model('', array('route' => array('turma.update', $turma->id), 'method' => 'PUT')) }}

              {{ csrf_field() }}
                 <div class="form-group">
                <label for="exampleFormControlInput1">Nome</label>
                
                <input type="text" class="form-control" required placeholder="Nome da Turma" value="{{$turma->nome}}"name="nm_turma">
                
                    {{Form::label('id_professor','Professor')}}
	             {{ Form::select('id_professor',$professor_id,null,['class'=>'form-control required' ]) }}
	             
	             
	             {{Form::label('id_disciplina','Disciplina')}}
	             {{ Form::select('id_disciplina',$disciplina_id,null,['class'=>'form-control required']) }}
	             
                    <input type="submit" value="Atualizar Disciplina" class="btn btn-info white"> 
                    {{Form::close()}}
                </div>
            </div>
            </div></div>
            
            @endsection