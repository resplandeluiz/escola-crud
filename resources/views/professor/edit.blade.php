@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
  <div class="card">
                <div class="card-header">Atualizar Professor</div>

                <div class="card-body">
                    {{ Form::model('', array('route' => array('professor.update', $professor->id), 'method' => 'PUT')) }}

                 {{ csrf_field() }}
                 
                 <div class="form-group">
                <label for="exampleFormControlInput1">Nome</label>
                <input type="text" class="form-control" required placeholder="Nome" name="nm_professor" value="{{$professor->nome}}">
                
                
                <label for="exampleFormControlInput1">Matrícula</label>
                <input type="text" class="form-control" required placeholder="Matricula" name="nr_matricula" value="{{$professor->matricula}}">
                
               
                  </div>
                    <input type="submit" value="Atualizar Professor" class="btn btn-info white"> 
                    {{Form::close()}}
                </div>
            </div>
            </div></div>
            
            @endsection