@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
  <div class="card">
                <div class="card-header">Cadastrar Professor</div>

                <div class="card-body">
                    {{ Form::open(array('route'=>'professor.store','method'=>'POST','id'=>'form')) }}

                 {{ csrf_field() }}
                 <div class="form-group">
                <label for="exampleFormControlInput1">Matrícula</label>
                
                <input type="text" class="form-control" required placeholder="Matrícula" name="nr_matricula">
                
                 <label for="exampleFormControlInput1">Nome</label>
                <input type="text" class="form-control" required placeholder="Nome completo" name="nm_professor">
                
               
                  </div>
                    <input type="submit" value="Cadastrar Professor" class="btn btn-info white"> 
                    {{Form::close()}}
                </div>
            </div>
            </div></div>
            
            
            <div class="row justify-content-center">
                <div class="col-md-8">
                      <div class="card">
                <div class="card-header">Professores Cadastrados</div>

                <div class="card-body">
                    <table class="table">
  <thead>
    <tr>
     
      <th scope="col">Id</th>
      <th scope="col">Nome</th>
      <th scope="col">Matrícula</th>
      <th scope="col">Data do Cadastro</th>
      <th scope="col"></th>
      <th scope="col"></th>
    
    </tr>
  </thead>
  <tbody>
    @forelse($professores as $professor)
    
    <tr>
      
      <td>{{$professor->id}}</td>
      <td>{{$professor->nome}}</td>
      <td>{{$professor->matricula}}</td>
      <td>{{$professor->created_at}}</td>
       <td>
          
          
          {{Form::open(['route'=>['professor.edit',$professor->id], 'method'=>'GET'])}}
           {{Form::submit('Editar', ['class'=>'btn btn-info btn-sm col-md-12'] )}}
  			{{Form::close()}}
        
    </td>
      <td>
          
          
          {{Form::open(['route'=>['professor.destroy',$professor->id], 'method'=>'DELETE'])}}
           {{Form::submit('Excluir', ['class'=>'btn btn-danger btn-sm col-md-12'] )}}
  			{{Form::close()}}
        
    </td>
          
          
      
      
    </tr>
    @empty
    
    Nenhum professor cadastrado!
    
    @endforelse
  </tbody>
</table>
                </div>
                
            </div>
            </div></div>
            
            
            
            
            
            
            </div>

  
  @endsection