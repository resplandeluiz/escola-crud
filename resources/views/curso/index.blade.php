@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
  <div class="card">
                <div class="card-header">Cadastrar Cursos</div>

                <div class="card-body">
                    {{ Form::open(array('route'=>'curso.store','method'=>'POST','id'=>'form')) }}

                 {{ csrf_field() }}
                 
                 <div class="form-group">
               
	            
                  {{Form::label('nome','Nome Curso')}}
	             {{ Form::text('nome','',['class'=>'form-control required']) }}
	             
	             {{Form::label('duracao','Semestres')}}
	             {{ Form::number('duracao','',['class'=>'form-control required']) }}
               
                  </div>
                    <input type="submit" value="Cadastrar curso" class="btn btn-info white"> 
                    {{Form::close()}}
                </div>
            </div>
            </div></div>
            
            
            <div class="row justify-content-center">
                <div class="col-md-8">
                      <div class="card">
                <div class="card-header">Cursos Cadastrados</div>

                <div class="card-body">
                    <table class="table">
  <thead>
    <tr>
     
      <th scope="col">Id</th>
      <th scope="col">Nome</th>
       <th scope="col">Semestres</th>
      <th scope="col"></th>
      <th scope="col"></th>
      
     
    
    </tr>
  </thead>
  <tbody>
    @forelse($cursos as $curso)
    
    <tr>
      
      <td>{{$curso->id}}</td>
      <td>{{$curso->nome}}</td>
      <td>{{$curso->duracao}}</td>
     
       <td>
          
          
          {{Form::open(['route'=>['curso.edit',$curso->id], 'method'=>'GET'])}}
          {{ csrf_field() }}
           {{Form::submit('Editar', ['class'=>'btn btn-info btn-sm col-md-12'] )}}
  			{{Form::close()}}
        
    </td>
      <td>
          
          
          {{Form::open(['route'=>['curso.destroy',$curso->id], 'method'=>'DELETE'])}}
          
          {{ csrf_field() }}
           {{Form::submit('Excluir', ['class'=>'btn btn-danger btn-sm col-md-12'] )}}
           
  			{{Form::close()}}
        
    </td>
          
          
      
      
    </tr>
    @empty
    
    Nenhum curso cadastrado!
    
    @endforelse
  </tbody>
</table>
                </div>
                
            </div>
            </div></div>
            
            
            
            
            
            
            </div>

  
  @endsection