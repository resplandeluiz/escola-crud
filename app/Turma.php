<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Turma extends Model
{
      # NOME DA TABELA  ############################################
    protected $table = 'turma';
     Use SoftDeletes;
    
    
    # ATRIBUTOS DA TABELA ########################################
    protected $fillable = ['id','nome','professor_id','disciplina_id'];
    
    
    //MÉTODO QUE REALIZA A JUNÇÃO ENTRE ALUNO, TURMA, PROFESSOR E DISCIPLINA
   public static function pegarTurmaToda(){
        
        return DB::table('turma')
        ->join('professor','professor.id','=','turma.professor_id')
        ->join('disciplina','disciplina.id','=','turma.disciplina_id')
        ->select('turma.*','professor.nome as professor_nome','disciplina.nome_disciplina as nome_disciplina')
        ->get();
        
    }
}
