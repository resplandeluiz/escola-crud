<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Professor extends Model
{
     # NOME DA TABELA  ############################################
    protected $table = 'professor';
     Use SoftDeletes;
    
    
    # ATRIBUTOS DA TABELA ########################################
    protected $fillable = ['id','matricula','nome'];
}
