<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Aluno extends Model
{
       # NOME DA TABELA  ############################################
    protected $table = 'aluno';
     Use SoftDeletes;
    
    
    # ATRIBUTOS DA TABELA ########################################
    protected $fillable = ['id','matricula','nome','idade','sexo'];
    
    
    
    
}
