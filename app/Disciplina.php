<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Disciplina extends Model
{
     # NOME DA TABELA  ############################################
    protected $table = 'disciplina';
    Use SoftDeletes;
    protected $data = ['deleted_at'];
    
    # ATRIBUTOS DA TABELA ########################################
    protected $fillable = ['id','nome_disciplina'];
}
