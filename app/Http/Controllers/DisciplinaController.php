<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Disciplina;
use App\Turma;
use Carbon;


class DisciplinaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
       $disciplinas =  Disciplina::all();
        return view('disciplina.index',compact('disciplinas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $arquivo = $request->file('foto');
       
        if( !$arquivo->getError() ){
        
            $arquivo->store('cursos');   
            
        }
        
        
        //Aqui recebe os dados que vem do FORMULARIO
        
        //REALIZA A VALIDAÇÃO
        $rules = array('nm_disciplina' => 'max:255|required');
        $validator = Validator::make($request->all(), $rules);
        
       
        if ($validator->fails()) {
            
            return Redirect::to('disciplina')->withErrors('Tente novamente!');
            
        }else{
            
          $disciplina = Disciplina::create([
              
              'nome_disciplina' => $request->nm_disciplina
              
              
              ]);
          
          
            //retorna mensagem de sucesso
            Session::flash('Cadastrado com Sucesso!');
            return Redirect::to('disciplina');
            
        }
        
      
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $disciplina = Disciplina::find($id);
        
        return view('disciplina.edit',compact('disciplina'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        //Realiza a validação
       $rules = array('nm_disciplina' => 'max:255|required');
        $validator = Validator::make($request->all(), $rules);
        
       
        if ($validator->fails()) {
            
            return Redirect::to('disciplina')->withErrors('Tente novamente!');
            
        }else{
            
           $disciplina = Disciplina::find($id);
           
           if(!isset($disciplina)){
               
               return Redirect::to('disciplina')->withErrors('Tente novamente!');
               
           }
        
            $disciplina->nome_disciplina = $request->nm_disciplina;
            $disciplina->save();
          
            //Retorna mensagem de sucesso
            Session::flash('message','Atualizado com Sucesso!');
            return Redirect::to('disciplina');
            
            
        }
       
        
        
        
       
      
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            
         $turma = Turma::where('disciplina_id','=',$id)->get();
        
        if(count($turma) > 0){
            
            return Redirect::to('disciplina')->withErrors('Esta disciplina ainda está em uma turma!');
        }else{
            
            Disciplina::destroy($id);
            Session::flash('message','Disciplina exlcuído com sucesso!');
            
            return Redirect::to('disciplina');
            
        }
            
            
    }
}
