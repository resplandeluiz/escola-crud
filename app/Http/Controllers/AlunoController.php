<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Aluno;
use App\Turma;

class AlunoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listTurmas = Aluno::all();
        
        //Adequando um array para um select
        $turmas = Turma::all();
        foreach($turmas as $turma){ $turma_id[$turma->id] = $turma->nome; }
        
        
        //VALIDANDO SE EXISTE TURMAS
        $existeTurma = count($turmas);
         if($existeTurma == 0){
            
            return Redirect::to('home')->withErrors('Precisa ter pelo menos uma turma para se cadastrar um aluno!');
        }
        
        //Chamando o método que traz o aluno, turma, professor, disicplina
        //UTILIZO ESTE (NEW ALUNO), pois só utilizo ele uma única vez
        $alunoComturma = (new Aluno)->alunoComTurma();
       
        
       return view('aluno.index',compact('listTurmas','turma_id','alunoComturma'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //REALIZA A VALIDAÇÃO
        $rules = array(
            
            'nm_aluno' => 'required|max:255',
            'nr_matricula' => 'required',
            'nr_idade' => 'required',
            'sexo' => 'required',
            'id_turma' => 'required|numeric',
        );
        
        $validator = Validator::make($request->all(), $rules);
        
      
        if ($validator->fails()) {
            
            return Redirect::to('aluno')->withErrors('Preencha os campos corretamente!');
            
        }else{
        
        
            $aluno = Aluno::create([
              
              'matricula' => $request->nr_matricula,
              'nome' => $request->nm_aluno,
              'idade' => $request->nr_idade,
              'sexo' => $request->sexo,
              'turma_id' => $request->id_turma,
             
              
              ]);
          
          
            //Retorna a mensagem de sucesso
            Session::flash('message','Cadastrado com Sucesso!');
            return Redirect::to('aluno');
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aluno = Aluno::find($id);
        
          //Adequando um array para um select
        $turmas = Turma::all();
        foreach($turmas as $turma){ $turma_id[$turma->id] = $turma->nome; }
        $existeTurma = count($turmas);
        
        //Validando se existe turma
         if($existeTurma == 0){
            
            return Redirect::to('home')->withErrors('Precisa ter pelo menos uma turma para se cadastrar um aluno!');
        }
        
        
      
        
        return view('aluno.edit',compact('aluno','turma_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        
        //REALIZA A VALIDAÇÃO
        $rules = array(
            
            'nm_aluno' => 'required|max:255',
            'nr_matricula' => 'required',
            'nr_idade' => 'required',
            'sexo' => 'required',
            'id_turma' => 'required|numeric',
        );
        
        $validator = Validator::make($request->all(), $rules);
        
       
        if ($validator->fails()) {
            
            return Redirect::to('aluno')->withErrors('Preencha os campos corretamente!');
            
        }else{
            
            
          $aluno = Aluno::find($id);
          $aluno->matricula = $request->nr_matricula;
          $aluno->nome = $request->nm_aluno;
          $aluno->idade = $request->nr_idade;
          $aluno->sexo = $request->sexo;
          $aluno->turma_id = $request->id_turma;
          
          $aluno->save();
            
           //Retorna mensagem de sucesso
            Session::flash('message','Atualizado com Sucesso!');
            return Redirect::to('aluno');
            
        }
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Aluno::destroy($id);
        
        //retorna mensagem de sucesso
        Session::flash('message','Aluno excluído com sucesso');
        return Redirect::to('/aluno');
        
    }
}
