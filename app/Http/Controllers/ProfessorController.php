<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Professor;
use App\Turma;

class ProfessorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $professores = Professor::all();
    
            return view('professor.index',compact('professores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Aqui recebe os dados que vem do FORMULARIO
        
        //REALIZA A VALIDAÇÃO
        $rules = array(
            
            'nr_matricula' => 'required|max:255',
            'nm_professor' => 'required'
        );
        
        $validator = Validator::make($request->all(), $rules);
        
       
        if ($validator->fails()) {
            
            return Redirect::to('professor')->withErrors('Preencha os campos corretamente!');
            
        }else{
            
            $professor = Professor::create([
              
              'matricula' => $request->nr_matricula,
              'nome' => $request->nm_professor
              
              ]);
          
            Session::flash('Cadastrado com Sucesso!');
            return Redirect::to('professor');
            
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $professor = Professor::find($id);
        return view('professor.edit',compact('professor'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            
           'nr_matricula' => 'max:255|required|max:255',
            'nm_professor' => 'required'
            );
        $validator = Validator::make($request->all(), $rules);
        
       
        if ($validator->fails()) {
            
            return Redirect::to('professor')->withErrors('Tente novamente!');
            
        }else{
            
           $professor = Professor::find($id);
           
           if($professor == null){
               
               return Redirect::to('professor')->withErrors('Tente novamente!');
               
           }
           
            $professor->matricula = $request->nr_matricula;
            $professor->nome = $request->nm_professor;
            
        
            $professor->save();
          
            Session::flash('message','Atualizado com Sucesso!');
            return Redirect::to('professor');
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $turma = Turma::where('professor_id','=',$id)->get();
        
        if(count($turma) > 0){
            
            return Redirect::to('professor')->withErrors('Este professor ainda possui uma turma!');
        }else{
            
            Professor::destroy($id);
            
            //retorna a mensagem de sucesso
            Session::flash('message','Professor exlcuído com sucesso!');
            
            
            return Redirect::to('professor');
            
        }
        
    }
}
