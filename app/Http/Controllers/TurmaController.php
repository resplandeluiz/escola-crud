<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;

use App\Turma;
use App\Professor;
use App\Disciplina;
use App\Aluno;

class TurmaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $turmas = Turma::all();
        
        $professores = Professor::all();
        foreach($professores as $prof){ $professor_id[$prof->id] = $prof->nome; }
     
        
        $disciplinas = Disciplina::all();
        foreach($disciplinas as $disciplina){ $disciplina_id[$disciplina->id] = $disciplina->nome_disciplina; }
        
        
        
        //VALIDA SE EXISTE PROFESSORES E DISCPLINAS PARA CADASTRAR UMA TURMA
        $existeProfessor = count($professores);
        $existeDisciplina = count($disciplinas);
        
        if($existeProfessor == 0 || $existeDisciplina == 0){
            
            return Redirect::to('home')->withErrors('Precisa ter pelo menos um professor e uma discplina para cadastrar uma turma!');
        }
         
        
        $listTurmas = (new Turma)->pegarTurmaToda();
       
        
        return view('turma.index',compact('turmas','professor_id','disciplina_id','listTurmas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //REALIZA A VALIDAÇÃO
        $rules = array(
            
            'nm_turma' => 'required|max:255',
            'id_professor' => 'required',
            'id_disciplina' => 'required'
        );
        
        $validator = Validator::make($request->all(), $rules);
        
       
        if ($validator->fails()) {
            
            return Redirect::to('turma')->withErrors('Preencha os campos corretamente!');
            
        }else{
            
            $turma = Turma::create([
              
              'nome' => $request->nm_turma,
              'professor_id' => $request->id_professor,
              'disciplina_id' => $request->id_disciplina,
              
              ]);
          
            Session::flash('Cadastrado com Sucesso!');
            return Redirect::to('turma');
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $turma = Turma::find($id);
        
        //Adequando um array para um select
        $professores = Professor::all();
        foreach($professores as $prof){ $professor_id[$prof->id] = $prof->nome; }
     
        //Adequando um array para um select
        $disciplinas = Disciplina::all();
        foreach($disciplinas as $disciplina){ $disciplina_id[$disciplina->id] = $disciplina->nome_disciplina; }
        
        
        
        //VALIDA SE EXISTE PROFESSORES E DISCPLINAS PARA CADASTRAR UMA TURMA
        $existeProfessor = count($professores);
        $existeDisciplina = count($disciplinas);
        
        if($existeProfessor == 0 || $existeDisciplina == 0){
            
            return Redirect::to('home')->withErrors('Precisa ter pelo menos um professor e uma discplina para editar uma turma!');
        }
        
        
        return view('turma.edit',compact('turma','disciplina_id','professor_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //REALIZA A VALIDAÇÃO
        $rules = array(
            
            'nm_turma' => 'required|max:255',
            'id_professor' => 'required',
            'id_disciplina' => 'required'
        );
        
        $validator = Validator::make($request->all(), $rules);
        
       
        if ($validator->fails()) {
            
            return Redirect::to('turma')->withErrors('Preencha os campos corretamente!');
            
        }else{
           
            $turma = Turma::find($id);
          
            $turma->nome = $request->nm_turma;
            $turma->professor_id = $request->id_professor;
            $turma->disciplina_id = $request->id_disciplina;
            
            $turma->save();
            
            Session::flash('Cadastrado com Sucesso!');
            return Redirect::to('turma');
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aluno = Aluno::where('turma_id','=',$id)->get();
        if(count($aluno) > 0){
            
            return Redirect::to('turma')->withErrors('Esta turma não pode ser deletada, pois pussui alunos cadastrados.');
        }else{
            
            Turma::destroy($id);
            Session::flash('message','Turma excluída com sucesso!');
            
            return Redirect::to('turma');
            
        }
        dd($aluno);
    }
}
