<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class Banco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        //CRIA TABELA DISCIPLINA
         Schema::create('disciplina', function (Blueprint $table) {
             
            $table->increments('id');
            $table->string('nome_disciplina',50);
            
            $table->integer('curso_id')->unsigned()->nullable();
            
            $table->foreign('curso_id')
                  ->references('id')
                  ->on('cursos');
            
            $table->softDeletes();
            $table->timestamps();
            
        });
        
        
        //CRIA TABELA PROFESSOR
         Schema::create('professor', function (Blueprint $table) {
             
            $table->increments('id')->unsigned();
            
            $table->integer('matricula')->unique();
            $table->string('nome',100);
            $table->string('telefone',15);
           $table->string('email',50);
           
             $table->string('cpf',15);
            $table->integer('cep')->nullable();
            $table->string('logradouro',100)->nullable();
            $table->string('complemento',100)->nullable();
            $table->string('bairro',100)->nullable();
            $table->char('uf',2)->nullable();
          $table->integer('municipio');
            $table->timestamp('data_nascimento');
            
            
            $table->softDeletes();
            $table->timestamps();
            
        });
        
        
        //CRIA TABELA PROFESSOR
         Schema::create('turma', function (Blueprint $table) {
             
            $table->increments('id');
            $table->string('nome');
            
            //FAZ REFERENCIA DA CHAVE ESTRANGEIRA DA TABELA PROFESSOR 
            $table->integer('professor_id')->unsigned()->nullable();
            $table->foreign('professor_id')
                  ->references('id')
                  ->on('professor');
            
       

            //FAZ REFERENCIA DA CHAVE ESTRANGEIRA DA TABELA DISCIPLINA                   
            $table->integer('disciplina_id')->unsigned()->nullable();
            $table->foreign('disciplina_id')
                  ->references('id')
                  ->on('disciplina');
                  
            $table->softDeletes();
            $table->timestamps();
            
        });
        
        
        //CRIA TABELA ALUNO
         Schema::create('aluno', function (Blueprint $table) {
             
            $table->increments('id');
            
            $table->integer('matricula')->unique();
            
            $table->string('nome',100)->nullable();
            $table->string('telefone',15);
            $table->string('email',50);
           
            $table->integer('cep')->nullable();
            $table->string('logradouro',100)->nullable();
            $table->string('complemento',100)->nullable();
            $table->string('bairro',100)->nullable();
            $table->char('uf',2)->nullable();
            $table->integer('municipio');
            $table->timestamp('data_nascimento')->nullable();
            
            //FAZ REFERENCIA DA CHAVE ESTRANGEIRA DA TABELA TURMA                   
            $table->integer('turma_id')->unsigned()->nullable();
            $table->foreign('turma_id')
                  ->references('id')
                  ->on('turma')
                  ->onDelete('cascade');
                  
            $table->softDeletes();
            $table->timestamps();
            
        });
        
    
        
 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        
        Schema::dropIfExists('aluno');
          Schema::dropIfExists('disciplina');
        

    }
}
