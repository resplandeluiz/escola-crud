<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTurmaAlunosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turma_alunos', function (Blueprint $table) {
            
            $table->increments('id')->unsigned();
            
            //FAZ REFERENCIA DA CHAVE ESTRANGEIRA DA TABELA PROFESSOR 
            $table->integer('turma_id')->unsigned()->nullable();
            $table->foreign('turma_id')
                  ->references('id')
                  ->on('turma');
                  
            $table->integer('aluno_id')->unsigned()->nullable();
            $table->foreign('aluno_id')
                  ->references('id')
                  ->on('aluno');
                  
            $table->integer('semestre');
        
            $table->softDeletes();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turma_alunos');
    }
}
