<?php

use Illuminate\Database\Seeder;

class CursoSeeder extends Seeder
{
    
    public function run()
    {
         DB::table('cursos')->insert([
             
            ['nome' => "ADS",'duracao' => 5],
            ['nome' => "Ciencia da computação",'duracao' => 10],
            ['nome' => "Engenharia de software",'duracao' => 10],
            ['nome' => "Jogos Digitais",'duracao' => 5],
            ['nome' => "Design",'duracao' => 5]
            
        ]);
    }
}
