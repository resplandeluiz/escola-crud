<?php

use Illuminate\Database\Seeder;

class DisciplinaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('disciplina')->insert([
             
            ['nome_disciplina' => "WEB II",'curso_id' => 1],
            ['nome_disciplina' => "Unity 3D",'curso_id' => 4],
            ['nome_disciplina' => "Cálculo 2",'curso_id' => 2],
            ['nome_disciplina' => "Diagramação",'curso_id' => 5],
            ['nome_disciplina' => "UML",'curso_id' => 3]
            
        ]);
    }
}
