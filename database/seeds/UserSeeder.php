<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            'name' => 'Luiz Resplande',
            'email' => 'luizresplande@gmail.com',
            'password' => bcrypt('123456')
           
        ]);
    }
}
